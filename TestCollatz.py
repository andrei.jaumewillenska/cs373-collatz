#!/usr/bin/env python3
"""
This is the module to run the unit tests for the collatz.py project

"""

# --------------
# TestCollatz.py
# --------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):

    """ This is the class to reun the unit tests for the project """
    # ----
    # read
    # ----

    def test_read(self):
        """ Simple method that tests reading from stdin """
        string = "1 10\n"
        i, j = collatz_read(string)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        """ First unit test from the hacker rank website """
        c_eval = collatz_eval(1, 10)
        self.assertEqual(c_eval, 20)

    def test_eval_2(self):
        """ Unit test also from hacker rank """
        c_eval = collatz_eval(100, 200)
        self.assertEqual(c_eval, 125)

    def test_eval_3(self):
        """ Unit test given to us from the start """
        c_eval = collatz_eval(201, 210)
        self.assertEqual(c_eval, 89)

    def test_eval_4(self):
        """ Last sample unit test from hacker rank """
        c_eval = collatz_eval(900, 1000)
        self.assertEqual(c_eval, 174)

    def test_eval_6(self):
        """ Unit test to check the same number as both bounds """
        c_eval = collatz_eval(1, 1)
        self.assertEqual(c_eval, 1)

    def test_eval_7(self):
        """ Unit test to calculate the cycle length of all the nums """
        c_eval = collatz_eval(1, 1000000)
        self.assertEqual(c_eval, 525)

    def test_eval_8(self):
        """ Check one of the optimizations """
        c_eval = collatz_eval(1, 1000000)
        c_eval_2 = collatz_eval(500001, 1000000)
        self.assertEqual(c_eval, c_eval_2)

    def test_eval_9(self):
        """ Unit test to check the eager cache """
        c_eval = collatz_eval(1, 100000)
        self.assertEqual(c_eval, 351)

    def test_eval_10(self):
        """ Test to check the functin without the eager cache """
        c_eval = collatz_eval(90000, 100000)
        self.assertEqual(c_eval, 333)

    # -----
    # print
    # -----

    def test_print(self):
        """ Unit test to print the results """
        string = StringIO()
        collatz_print(string, 1, 10, 20)
        self.assertEqual(string.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        """ Run through the enitre code """
        input_string = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        writer = StringIO()
        collatz_solve(input_string, writer)
        self.assertEqual(
            writer.getvalue(),
            "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        """ Run through the enitre code backwards """
        input_string = StringIO("10 1\n200 100\n210 201\n1000 900\n")
        writer = StringIO()
        collatz_solve(input_string, writer)
        self.assertEqual(
            writer.getvalue(),
            "10 1 20\n200 100 125\n210 201 89\n1000 900 174\n")

    def test_all(self):
        """ Test all the possible numbers """
        input_string = StringIO("1 1000000\n")
        writer = StringIO()
        collatz_solve(input_string, writer)
        self.assertEqual(writer.getvalue(), "1 1000000 525\n")
# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
