#!/usr/bin/env python3
"""
Module to actually perform the cycle length of a number as described in the
Collatz conjecture

"""

# ----------
# Collatz.py
# ----------

import math
from typing import IO, List, Dict

# ----------------
# Global Variables
# ----------------

CACHE: Dict[int, int] = {}
EAGER_CACHE: Dict[int, int] = {}

# --------------
# populate_eager_cache
# --------------


def populate_eager_cache():
    """  Lazy eager_cache that stores cycle length for a number  """
EAGER_CACHE = {160000: 383, 640000: 416, 940000: 507, 210000: 373, 10000: 260,
               110000: 354, 320000: 384, 740000: 424, 950000: 383, 820000: 450,
               430000: 418, 330000: 384, 190000: 365, 540000: 377, 750000: 393,
               960000: 414, 20000: 279, 370000: 410, 440000: 400, 650000: 416,
               680000: 385, 380000: 423, 390000: 436, 340000: 366, 550000: 452,
               760000: 424, 970000: 414, 30000: 308, 530000: 408, 240000: 443,
               450000: 369, 660000: 429, 870000: 388, 140000: 344, 350000: 441,
               700000: 442, 90000: 333, 560000: 421, 770000: 468, 980000: 458,
               40000: 324, 250000: 368, 460000: 387, 670000: 442, 230000: 368,
               880000: 432, 200000: 360, 150000: 375, 360000: 379, 570000: 421,
               220000: 386, 780000: 437, 990000: 427, 80000: 351, 840000: 525,
               260000: 363, 890000: 445, 860000: 419, 730000: 411, 790000: 406,
               1000000: 440, 60000: 340, 480000: 382, 420000: 449, 630000: 509,
               900000: 370, 170000: 370, 590000: 434, 800000: 468, 270000: 407,
               50000: 314, 280000: 407, 490000: 413, 70000: 335, 910000: 445,
               180000: 347, 120000: 349, 580000: 390, 690000: 398, 600000: 403,
               810000: 406, 290000: 389, 500000: 426, 130000: 344, 710000: 504,
               920000: 476, 400000: 405, 610000: 403, 620000: 447, 850000: 419,
               300000: 371, 510000: 426, 720000: 411, 930000: 476, 470000: 444,
               410000: 405, 830000: 450, 100000: 333, 520000: 470, 310000: 371}

populate_eager_cache()

# ------------
# collatz_read
# ------------


def collatz_read(string: str) -> List[int]:
    """
    read two ints s a string and returns a list of two ints,
    representing the beginning and end of a range, [i, j]

    """
    line = string.split()
    return [int(line[0]), int(line[1])]


def run_collatz(i: int, steps: int) -> int:
    """
    actually find the max value of the cycle length

    """
    assert i > 0

    if i <= 1:
        return steps

    if CACHE.get(i) is not None:
        return CACHE[i]

    new_int = i >> 1
    val = 0

    if i % 2 == 0:
        val = steps + run_collatz(new_int, 1)
    else:
        val = steps + 1 + run_collatz(
            i + new_int + 1,
            1)       # Odd number opt

    CACHE[i] = val
    return CACHE[i]

# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end of the range, inclusive
    return the max cycle length of the range [i, j]

    """
    max_val = 1
    cycles_size = 10000

    mid = (j // 2) + 1
    if i >= mid:
        mid = i

    assert i < 1000001
    assert j < 1000001
    assert mid < 1000001

    if j - mid > 10000:

        low = math.floor(mid / cycles_size)
        high = math.floor(j / cycles_size)

        # Check the ranges of the eager CACHE
        for num in range(mid, ((low + 1) * cycles_size) + 1):
            temp = run_collatz(num, 1)
            max_val = temp if max_val <= temp else max_val

        for num in range(low + 2, high + 1):
            if num < 100 and EAGER_CACHE.get(num * cycles_size) is not None:
                temp = EAGER_CACHE[num * cycles_size]
                max_val = temp if max_val <= temp else max_val

        for num in range(high * cycles_size, j + 1):
            temp = run_collatz(num, 1)
            max_val = temp if max_val <= temp else max_val
    else:

        for num in range(mid, j + 1):
            temp = run_collatz(num, 1)
            max_val = temp if max_val <= temp else max_val

    return max_val

# -------------
# collatz_print
# -------------


def collatz_print(writer: IO[str], i: int, j: int, val: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end of the range, inclusive
    v the max cycle length

    """
    writer.write(str(i) + " " + str(j) + " " + str(val) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(reader: IO[str], writer: IO[str]) -> None:
    """
    reader a reader
    writer a writer

    """
    for string in reader:
        i, j = collatz_read(string)
        lower = i
        higher = j

        if i > j:        # Run always in an increaseing order
            lower = j
            higher = i

        c_eval = collatz_eval(lower, higher)
        collatz_print(writer, i, j, c_eval)        # Prin in the correct order
