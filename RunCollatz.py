#!/usr/bin/env python3
"""
This is the run harness module for the Collatz.py file

"""

# -------------
# RunCollatz.py
# -------------

# -------
# imports
# -------

from sys import stdin, stdout
from Collatz import collatz_solve

# ----
# main
# ----

# Run the collatz_solve function from Collatz.py
if __name__ == "__main__":
    collatz_solve(stdin, stdout)
